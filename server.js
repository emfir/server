import handleSignIn from "./controllers/signin.js";
import handleRegister from "./controllers/register.js";
import express from "express";
import cors from "cors";
import bcrypt from "bcrypt";
import knex from "knex";
import handleImage from "./controllers/image.js";
import handleProfile from "./controllers/profile.js";

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const db = knex({
    client: 'pg',
    connection: {
        connectionString: process.env.DATABASE_URL,
        ssl:true
    }
});


const app = express();

app.use(express.json())
app.use(cors())
app.post('/signin', (req, res) =>
    handleSignIn(req, res, bcrypt, db));
app.post('/register', (req, res) =>
    handleRegister(req, res, bcrypt, db));
app.put('/image', (req, res) =>
    handleImage(req, res, db))
app.get('/profile/:id', (req, res) =>
    handleProfile(req, res, db))
app.get('/', ((req, res) => {
    db.select('*').from('users').then(users => res.json(users));
}))

app.listen(process.env.PORT || 3000, () => {
    console.log(`app is running on port ${process.env.PORT}`);
})