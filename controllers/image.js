const handleImage = (req, res, db) => {
    const {id} = req.body;
    db('users')
        .returning('entries')
        .where({id: id})
        .increment({entries: 1})
        .then(data => {
            if (data.length) {
                res.json(data[0])
            } else {
                res.status(404).json("no user with such id")
            }
        })

}

export default handleImage