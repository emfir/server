const handleRegister = (req, res, bcrypt, db) => {
    const {email, name, password} = req.body;
    if (!email || !name || !password) {
        return res.status(400).json("not correct inputs")
    }

    const salt = bcrypt.genSaltSync();
    const hash = bcrypt.hashSync(password, salt);

    db.transaction(trx => {
        trx.insert({hash: hash, email: email})
            .into('login')
            .then(data => trx('users')
                .returning('*')
                .insert({
                    name: name,
                    email: email,
                    joined: new Date()
                })
                .then(user => res.json(user[0]))
                .catch(err => res.status(400).json('user already exists')))
            .then(trx.commit)
            .catch(trx.rollback);
    }).then(function (inserts) {
        console.log('new user registered.');
    }).catch(err => res.status(400).json(err));

};

export default handleRegister