const handleSignIn = (req, res, bcrypt, db) => {
    const {email, password} = req.body;

    db.select('email', 'hash')
        .from('login')
        .where({email: email})
        .then(data => {
            if (bcrypt.compareSync(password, data[0].hash)){
                return db.select('*')
                    .from('users')
                    .where({email:email})
                    .then(user => res.json(user[0]))

            }else{
                return res.status(400).json("wrong credentials")
            }
        }).catch(err => res.status(400).json("wrong credentials"))


}

export default handleSignIn